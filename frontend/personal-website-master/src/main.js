import './styles/style.css';

const emailInput = document.getElementById('email');
const joinForm = document.getElementById('joinForm');
const unsubscribeContainer = document.getElementById('unsubscribeContainer');
const unsubscribeButton = document.getElementById('unsubscribeButton');

function saveEmailToLocalStorage() {
  const emailValue = emailInput.value;
  localStorage.setItem('subscriptionEmail', emailValue);
}

emailInput.addEventListener('input', saveEmailToLocalStorage);

function populateEmailFromLocalStorage() {
  const storedEmail = localStorage.getItem('subscriptionEmail');
  if (storedEmail) {
    emailInput.value = storedEmail;
  }
}

populateEmailFromLocalStorage();

function hideEmailInputAndShowButton() {
  const isValid = emailInput.checkValidity();
  if (isValid) {
    emailInput.style.display = 'none';
    unsubscribeContainer.innerHTML = `
      <button type="button" id="unsubscribeButton" disabled>Unsubscribe</button>
    `;
    emailInput.addEventListener('input', saveEmailToLocalStorage);
  }
}

async function handleUnsubscribe() {
  try {
    unsubscribeButton.disabled = true;
    unsubscribeButton.style.opacity = '0.5';
    const response = await fetch('/unsubscribe', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ email: emailInput.value }),
    });

    if (response.ok) {
      emailInput.style.display = 'block';
      unsubscribeContainer.innerHTML = '';
      unsubscribeButton.style.display = 'none'; 
      joinForm.style.display = 'block'; 
      localStorage.removeItem('subscriptionEmail');
    } else {
      const errorMessage = document.createElement('p');
      errorMessage.textContent = 'Failed to unsubscribe: ' + response.statusText;
      errorMessage.style.color = 'red';
      unsubscribeContainer.appendChild(errorMessage);
    }
  } catch (error) {
    console.error('An error occurred:', error);
  } finally {
    unsubscribeButton.disabled = false;
    unsubscribeButton.style.opacity = '1';
  }
}


async function handleFormSubmit(e) {
  e.preventDefault();

  try {
    const response = await fetch('/subscribe', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ email: emailInput.value }),
    });

    if (response.ok) {
      hideEmailInputAndShowButton();
      joinForm.style.display = 'none';
    } else if (response.status === 422) {
      const errorData = await response.json();
      const errorContainer = document.getElementById('errorContainer');
      errorContainer.textContent = errorData.error;
      errorContainer.style.display = 'block'; 
    } else {
      console.error('Failed to subscribe:', response.statusText);
    }
  } catch (error) {
    console.error('An error occurred:', error);
  }
}




// function displayErrorMessage(message) {
//   const errorMessage = document.createElement('div');
//   errorMessage.className = 'error-message';
//   errorMessage.textContent = message;
//   joinForm.appendChild(errorMessage);
// }


joinForm.addEventListener('submit', handleFormSubmit);


window.addEventListener('DOMContentLoaded', fetchCommunityData);

async function fetchCommunityData() {
  try {
    const response = await fetch('/community');
    if (response.ok) {
      const communityData = await response.json();

      console.log('Community Data:', communityData);
    } else {
      console.error('Failed to fetch community data:', response.statusText);
    }
  } catch (error) {
    console.error('An error occurred:', error);
  }
}

import { expect } from 'chai';
import sinon from 'sinon'; 
import { validate, validateAsync, validateWithLog, validateWithThrow } from '../email-validator.js';

describe('Email Validator', () => {
  it('should validate a valid email', () => {
    const validEmail = 'example@gmail.com'; 
    const isValid = validate(validEmail);
    expect(isValid).to.equal(true);
  });

  it('should invalidate an invalid email', () => {
    expect(validate('invalid-email')).to.be.false;
    expect(validate('user@')).to.be.false;
  });

  it('should invalidate an empty email', () => {
    expect(validate('')).to.be.false;
  });

  it('should invalidate an email with only spaces', () => {
    expect(validate('    ')).to.be.false;
  });

  it('should invalidate a null email', () => {
    expect(validate(null)).to.be.false;
  });

  it('should invalidate an undefined email', () => {
    expect(validate(undefined)).to.be.false;
  });

  it('should resolve to true for a valid email asynchronously', async () => {
    const validEmail = 'example@gmail.com'; 
    const isValid = await validateAsync(validEmail);
    expect(isValid).to.equal(true);
  });

  it('should resolve to false for an invalid email asynchronously', async () => {
    const invalidEmail = 'invalid-email';
    const isValid = await validateAsync(invalidEmail);
    expect(isValid).to.equal(false);
  });

  it('should resolve to false for an empty email asynchronously', async () => {
    const emptyEmail = '';
    const isValid = await validateAsync(emptyEmail);
    expect(isValid).to.equal(false);
  });

  it('should throw an error for an invalid email', () => {
    expect(() => validateWithThrow('invalid-email')).to.throw(Error);
    expect(() => validateWithThrow('user@')).to.throw(Error);
  });

  it('should log the validation result', () => {
    const consoleLogSpy = sinon.spy(console, 'log');
    const email = 'test@example.com'; 
    const isValid = validateWithLog(email);
    
    expect(consoleLogSpy.calledOnce).to.be.true;
    expect(consoleLogSpy.calledWith(`Email: ${email}, Valid: ${isValid}`)).to.be.true;
    
    consoleLogSpy.restore(); 
  });
});
